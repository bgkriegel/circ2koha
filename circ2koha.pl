#!/usr/bin/perl
$|++;

# Copyright (C) 2018 Bernardo Gonzalez Kriegel

# circ2koha.pl
# reads a file of circulation data, '|' separated
# 'op|date|time|cardnumber|barcode|date_due'
# op in ('i','r'), issue and return

use Modern::Perl;
use Getopt::Long;
use open qw(:std :encoding(UTF-8));

use Koha::Patrons;
use Koha::Items;
use C4::Context;
use C4::Items;

use Data::Printer;

my $file    = '';
my $debug   = 0;
my $verbose = 0;
my $insert  = 0;
my $force   = 0;
GetOptions(
    'file=s'   => \$file,
    'insert'   => \$insert,
    'force'    => \$force,
    'debug'    => \$debug,
    'verbose'  => \$verbose,
);
die("Must specify --file") if ( not ($file) );

my $issue_id;
my $issues;
my $old_issues;
my $borrowers;
my $items;
my $biblios;
my $issue_count;
my $count;
my $errors = 0;
my $branchcode;

open my $circulation, "<", $file;
while ( my $circ = <$circulation> ) {
    $count++;
    chomp $circ;
    my ( $op, $date, $hour, $cardnumber, $barcode, $due ) = split ( /\|/, $circ );

    # find borrower
    if ( not $borrowers->{ $cardnumber } ) {
        my $patron = Koha::Patrons->find( { cardnumber => $cardnumber } );
        if ( $patron ) {
           $borrowers->{ $cardnumber } = $patron->borrowernumber;
           $branchcode = $patron->branchcode; # unless ( $branch );
        }
    }

    # find item
    if ( not $items->{ $barcode } ) {
        my $item = Koha::Items->find({ barcode => $barcode });
        if ( $item ) {
           $items->{ $barcode }   = $item->itemnumber; 
           $biblios->{ $barcode } = $item->biblionumber; 
        }
    }

    # date & branch
    my $dt = {
        branchcode => $branchcode,
        timestamp  => "$date $hour",
        issuedate  => "$date $hour",
        date_due   => "$due $hour",
        date       => $date,
        due        => $due,
    };

    # issue
    if ( $op eq 'i' ) {                                
        if ( is_issued( $barcode ) ) {
            ($debug) and print "WARN  Already issued $barcode $dt->{timestamp}\n";
            do_return( $barcode, $dt );
        }
        if ( $items->{ $barcode } and $borrowers->{ $cardnumber } ) {
            do_issue( $barcode, $cardnumber, $dt );    
        } else {
            ( $debug ) and print "ERROR No barcode $barcode\n"       if ( not $items->{ $barcode }        );
            ( $debug ) and print "ERROR No cardnumber $cardnumber\n" if ( not $borrowers->{ $cardnumber } );
            $errors++;
        }        
    }
    # return
    elsif ( $op eq 'r' ) {
        if ( is_issued( $barcode ) ) {
            do_return( $barcode, $dt );
        } else {
            ($debug) and print "WARN  Not issued $barcode $dt->{timestamp}\n";
        }
    }

    ($verbose) and show_dots( $count );
}
close $circulation;

($debug) and print  "\nSummary for $branchcode\n";
($debug) and printf "Total issues   = %d\n", $issue_id;
($debug) and printf "Old issues     = %d\n", scalar keys %$old_issues;
($debug) and printf "Current issues = %d\n", scalar keys %$issues;
($debug) and printf "Borrowers      = %d\n", scalar keys %$borrowers;
($debug) and printf "Items          = %d\n", scalar keys %$items;
($debug) and printf "Errors         = %d\n\n", $errors;

exit 1 unless ( $insert );
if ( $errors and $insert and not $force ) {
    print "\nNOTICE: There are errors! To insert add '--force'\n";
    exit 2;
}

# use database
my $dbh = C4::Context->dbh;

# truncate issues and old_issues
my $truncate = $dbh->prepare("TRUNCATE old_issues");
$truncate->execute() or die "Can't truncate old_issues tables:".$truncate->errstr;
$truncate = $dbh->prepare("TRUNCATE issues");
$truncate->execute() or die "Can't truncate issues tables:".$truncate->errstr;

# insert old_issues
my $old_issue = $dbh->prepare("INSERT INTO old_issues ( issue_id, borrowernumber, itemnumber, date_due, branchcode, returndate, timestamp, issuedate ) VALUES ( ?,?,?,?,?,?,?,? )");
foreach my $issue_id ( sort { $a <=> $b } keys %$old_issues ) {
    my $p = $old_issues->{ $issue_id };
    $old_issue->execute( 
        $p->{issue_id}, 
        $p->{borrowernumber}, 
        $p->{itemnumber}, 
        $p->{date_due}, 
        $p->{branchcode}, 
        $p->{returndate}, 
        $p->{timestamp}, 
        $p->{issuedate} 
    ) or die "Can't insert old_issue: ".$old_issue->errstr;
}
print "Old issues inserted!\n";

# update issues count
my $issue_count_update = $dbh->prepare("UPDATE items SET issues = ? WHERE itemnumber = ?");
foreach my $itemnumber ( sort { $a <=> $b } keys %$issue_count ) {
    $issue_count_update->execute( 
        $issue_count->{ $itemnumber }, 
        $itemnumber 
    ) or die "Can't update issue count: ".$issue_count_update->errstr;
}
print "Issues count updated!\n";


# insert issues
my $curr_issues = $dbh->prepare("INSERT IGNORE INTO issues ( issue_id, borrowernumber, itemnumber, date_due, branchcode, timestamp, issuedate ) VALUES ( ?,?,?,?,?,?,? )");
foreach my $barcode ( sort { $issues->{$a}->{issue_id} <=> $issues->{$b}->{issue_id} } keys %$issues ) {
    my $p = $issues->{ $barcode };
    $curr_issues->execute(
            $p->{issue_id},
            $p->{borrowernumber},
            $p->{itemnumber},
            $p->{date_due},
            $p->{branchcode},
            $p->{timestamp},
            $p->{issuedate}
    ) or die "Can't insert issue: ".$curr_issues->errstr;

    ModItem( 
        { 
            issues           => $issue_count->{ $p->{itemnumber} },
            datelastborrowed => $p->{date},
            onloan           => $p->{due},
        }, 
        $biblios->{ $barcode }, 
        $items->{ $barcode }, 
    );
}
print "Current issues inserted!\n";

print "Finish!\n";

exit;

sub is_issued {
    my $barcode = shift;

    return ( $issues->{$barcode} )? 1 : 0 ;
}

sub do_issue {
    my $barcode    = shift;
    my $cardnumber = shift;
    my $dt         = shift;

    my $borrowernumber = $borrowers->{ $cardnumber };
    my $itemnumber     = $items->{ $barcode };

    $issue_id++;
    $issue_count->{ $itemnumber }++;

    $issues->{ $barcode } = { 
        issue_id       => $issue_id,
        borrowernumber => $borrowernumber,
        itemnumber     => $itemnumber,
        date_due       => $dt->{date_due},
        branchcode     => $dt->{branchcode},
        timestamp      => $dt->{timestamp},
        issuedate      => $dt->{issuedate},
        date           => $dt->{date},
        due            => $dt->{due},
    }
}

sub do_return {
    my $barcode    = shift;
    my $dt         = shift;

    $issues->{ $barcode }->{returndate} = $dt->{timestamp};
    $issues->{ $barcode }->{timestamp}  = $dt->{timestamp};
    $issues->{ $barcode }->{date}       = $dt->{date};

    $old_issues->{ $issues->{ $barcode }->{issue_id} } = $issues->{ $barcode };

    delete $issues->{ $barcode };
}

sub show_dots {
  my $count = shift;

  print "." if ( $count % 100 == 0  && $count % 1000 != 0 );
  print "|" if ( $count % 1000 == 0 );
  printf " %08d\n", $count if ( $count % 5000 == 0 );
}

exit;

