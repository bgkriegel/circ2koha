Circulation to Koha data migration tool
---------------------------------------

Run script as 'koha' user, inside koha-shell.
Gets branchcode value from patron information.

Arguments:

    --file      circulation data file, fixed format: 6 columns 
                (operation, date, hour, cardnumber, barcode, date_due)
                operation: 'i' for issue, 'r' for return
                date: iso date of operation
                hour: hour of operation, used for timestamps
                cardnumber and barcode: valid ones. If invalid operation is ignored
                date_due: iso date for issues, ignored for returns
                See sample data

    --insert    Needed to insert data
                WARNING: issues and old_issues tables are TRUNCATED

    --force     Needed to insert data in case of errors (invalid barcode/cardnumber)

    --debug     Prints informational messages
    --verbose   Prints activity messages

Example: 
    perl ~/circ2koha.pl --file /tmp/circ-test.txt --debug --insert --force
