Install
-------

Dependencies: 
* Modern::Perl ( apt-get install lib-modern-perl-perl )
* Koha

Copy the file to some directory in your path, e.g. /usr/local/bin
Give it execution permissions ( chmod +x )
